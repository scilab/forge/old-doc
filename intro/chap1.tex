% Copyright INRIA

\chapter{Introduction}

\section{What is Scilab}

Developed at INRIA and ENPC, Scilab has been developed for engineering 
applications. It is freely distributed (see the copyright file) and the source
code is available.

Scilab is made of three distinct parts: an interpreter, 
a set of libraries of Fortran and C routines linked with Scilab
and a number of toolboxes written in Scilab language.
The numerical librairies (which, strictly speaking, do not belong to Scilab but
are interactively called by the interpreter) are of 
independent interest and most of them are available through the Web. 
 
A key feature of the Scilab syntax is its ability to handle matrices: 
basic matrix manipulations such as concatenation, 
extraction or transpose are immediately performed as well as basic operations
such as addition or multiplication. The syntax for manipulating
matrices is mostly compatible with Matlab. 
Scilab also aims at handling more complex
objects than numerical matrices. This is
done in Scilab by manipulating structures which allows a 
natural symbolic representation of complicated
objects such as transfer functions, linear systems or graphs
(see Section~\ref{s2.5}).

 Mathematical objects such as polynomials, polynomials matrices 
and rational polynomial matrices are also defined and the syntax 
used for manipulating these matrices is identical to that used for 
manipulating numerical vectors and matrices.

        Scilab provides a variety of powerful primitives for
the analysis of non-linear systems.  
Integration of explicit and implicit dynamic systems can be accomplished 
numerically.  The {\tt scicos} toolbox allows the graphic definition
and simulation of complex interconnected hybrid systems. Documentation
about {\tt scicos} can be found at its the Web page 
\href{http://www.scicos.org}{scicos.org}.

 There exist numerical optimization facilities for non linear 
optimization (including non differentiable optimization), quadratic 
optimization and linear optimization (see also the contribution 
directory on the Scilab Web page).

  Scilab has an open programming environment where the
creation of functions and libraries of functions is completely in the
hands of the user (see Chapter~\ref{ch4}).    
Functions are recognized as data objects in Scilab and, thus, can be 
manipulated or created as other data objects.  For example, functions
can be defined inside Scilab and passed as input or output 
arguments of other functions.

In addition Scilab supports a character string data type 
which, in particular, allows the on-line creation of functions.
Matrices of character strings are also manipulated with the same
syntax as ordinary matrices.

        Finally, Scilab is easily interfaced with Fortran or C 
subprograms.  This allows use of standardized 
packages and libraries in the interpreted environment of Scilab.

        The general philosophy of Scilab is to provide the following
sort of computing environment:
\begin{itemize}
   \item To have data types which are varied and flexible with 
a syntax which is natural and easy to use.
   \item To provide a reasonable set of primitives which serve
           as a basis for a wide variety of calculations.
   \item To have an open programming environment where new
           primitives are easily added. 
   \item To support library development through ``toolboxes'' of
         functions devoted to specific
           applications (linear control, signal processing, 
           network analysis, non-linear control, etc.)
\end{itemize}

        The objective of this introduction manual is to give the user 
an idea of what Scilab can do. On line documentation about all
functions is available ({\tt help} command).


\section{Software Organization}

Scilab is divided into a set of directories. The main directory
\verb!SCIDIR! contains the following files: {\tt scilab.star} (startup file), the
copyright file {\tt notice.tex}, and the \verb!configure! files
(see(\ref{install})).
The main subdirectories are the following:
\begin{itemize}

\item{{\tt bin} is the directory of the executable files.
The starting script {\tt scilab} on Unix/Linux systems and {\tt runscilab.exe} on Windows.
The executable code of Scilab: {\tt scilex} on Unix/Linux systems and {\tt scilex.exe} on 
Windows are there. 
This directory also contains Shell scripts 
for managing or printing Postscript/\LaTeX\  files produced by Scilab.}

\item{{\tt demos} is the directory of demos. 
This directory contains the codes corresponding
to various demos. They are often useful for inspiring new users.
The file 
{\tt alldems.dem} is used by the  ``Demos'' button. 
Most of plot commands are illustrated by simple demo examples. 
Note that running a graphic function without input parameter
provides an example of use for this function (for instance {\tt
plot2d()} displays an example for using {\tt plot2d}  function). }

\item{{\tt examples} contains examples of specific topics. It is shown
in appropriate subdirectories how to add new C or Fortran program
to Scilab (see \verb!addinter-tutorial!). More complex examples are given
in \verb!addinter-examples!. The directory \verb!mex-examples!
contains examples of interfaces realized by emulating the Matlab
mexfiles. The directory \verb!link-examples! illustrates the use of
the \verb!call! function which allows to call external function within
Scilab.}

\item{{\tt macros} contains the libraries of functions
which are available on-line. New libraries can easily be added 
(see the Makefile). This directory is divided into a number of subdirectories
which contain ``Toolboxes'' for control, signal processing, etc... Strictly
speaking Scilab is not organized in toolboxes : functions of a specific
subdirectory can call functions of other directories; so, for example, the 
subdirectory {\tt signal} is not self-contained but all the functions there 
are devoted to signal processing.}

\item{{\tt man} is the directory containing the manual divided 
into submanuals, corresponding to the on-line help.}

To get information about an item, one should enter 
{\tt help item}\index{help@{\tt help}} 
in Scilab or use the help window facility obtained with help button.
To get information corresponding to a key-word, one should  enter 
{\tt apropos key-word} or use 
{\tt apropos}\index{apropos@{\tt apropos}} in the help window. 
All the {\tt item}s and {\tt key-words} known by the {\tt help} and 
{\tt apropos} commands are in {\tt .html} and {\tt whatis} files 
located in the {\tt man} subdirectories.

To add new items to the {\verb!help!} and {\tt apropos} commands 
the user can extend the list of directories available to the help 
browser by adapting the variable \verb!%helps!. 

\item{{\tt routines} is a directory which contains the source code of all
the numerical routines. The subdirectory {\tt default} is important
since it contains the
source code of routines which are necessary to customize Scilab.
In particular user's C or Fortran routines for ODE/DAE simulation 
or optimization can be included here (they can be also dynamically
linked)}

\item{{\tt tests} : this directory contains evaluation programs for testing 
Scilab's installation on a machine. The file ``demos.tst'' tests all the 
demos.}

\item{{\tt intersci} contains a program which can be used to build
interface programs for adding new Fortran or C 
primitives to Scilab. This program is executed by the {\tt intersci}
script in the {\tt bin/intersci} directory.}

\end{itemize}

\section{Installing Scilab. System Requirements}
\label{install}
Scilab is distributed in source code format; binaries for Windows98/NT/XP
systems and several popular Unix/Linux-XWindow systems are also
available. See the Scilab Web page and the contributions for specific
ports. All of these binaries versions include tk/tcl interface.

The installation requirements are the following :

- for the source version: Scilab requires approximately 130Mb of disk
storage to unpack and install (all sources included).  You a C 
compiler and a Fortran compiler.
  
- for the binary version: the minimum for running Scilab (without
sources) is about 40 Mb when decompressed.

\label{stks}
Scilab uses a large internal stack for its calculations. This size
of this stack can be reduced or enlarged by the 
{\tt stacksize}\index{stacksize@{\tt stacksize}}.
command. The default dimension of the internal stack can be adapted
by modifying the variable {\tt newstacksize} in the {\tt scilab.star}
script.

- For more information on the installation, please look at the README files.

\section{Documentation}

The documentation is made of this User's guide (Introduction to
Scilab) and the Scilab on-line manual. There are also
reports devoted to specific toolboxes: Scicos (graphic system builder
and simulator), Signal (Signal processing toolbox), Lmitool (interface for 
LMI problems), Metanet (graph and network toolbox). An FAQ is
available at Scilab home page:\\
(\verb!http://www.scilab.org!)\index{home page}.

Several documents are available in French, German, Spanish, Chinese etc. 
See the Scilab Web page.

\section{Scilab at a Glance. A Tutorial}

\subsection{Getting Started}

After starting Scilab, you get:

\bigskip

\begin{verbatim}
                           ===========
                           S c i l a b
                           ===========
 
 
                          Scilab-x.y.z
                  Copyright (C) 1989-xx INRIA/ENPC 
 
 
Startup execution:
  loading initial environment
   
 -->

\end{verbatim}


A first contact with Scilab can be made by clicking 
on {\tt Demos} with the left mouse button and clicking then on 
{\tt Introduction to SCILAB }: the 
execution of the session is then done by entering empty lines and can be
stopped with the buttons {\tt Stop} and {\tt Abort}.

  Several libraries
(see the {\tt SCIDIR/scilab.star} file) are automatically loaded.
        
To give the user an idea of some of the capabilities of Scilab
we will give later a sample session in Scilab.\\

\subsection{Editing a command line}

Before the sample session, we briefly present how to edit a command line.
You can enter a command line by typing after the prompt or clicking with the 
mouse on a part on a window and copy it at the prompt in the Scilab
window. 
The pointer may be moved using the directionnal arrows
($\leftarrow^\uparrow_\downarrow \rightarrow$). For Emacs customers,
the usual Emacs commands are at your 
disposal for modifying a command (Ctrl-$<$chr$>$  means hold the CONTROL key 
while typing the character $<$chr$>$), for example:

\bigskip


%
\begin{itemize}
\item Ctrl-p    recall previous line
\item Ctrl-n    recall next line
\item Ctrl-b    move backward one character
\item Ctrl-f    move forward one character
\item Delete    delete previous character
\item Ctrl-h    delete previous character
\item Ctrl-d    delete one character (at cursor)
\item Ctrl-a    move to beginning of line
\item Ctrl-e    move to end of line
\item Ctrl-k    delete to the end of the line
\item Ctrl-u    cancel current line
\item Ctrl-y    yank the text previously deleted
\item !{\tt prev}     recall the last command line which begins by {\tt prev}
\item Ctrl-c    interrupt Scilab and pause after carriage return. 
Clicking on the Control/stop button enters a Ctrl-c.
\end{itemize}
%

As said before you can also cut and paste using the mouse. This way will be
useful if you type your commands in an editor. Another way to 
``load'' files containing Scilab statements
is available with the {\tt File/File Operations} button.

\subsection{Sample Session for Beginners}

We present now some simple commands. At the carriage return all the 
commands typed since the last prompt are interpreted. 

\noindent\dotfill

\input{diary/d1p1.dia}

Give the values of 1 and 2 to the variables {\tt a} and { \tt A} .  The 
semi-colon at the end of the command suppresses the display of the result.
Note that Scilab is case-sensitive.
Then two commands are processed and the second result is displayed because
it is not followed by a semi-colon. The last command shows how to write a
command on several lines by using ``{\tt ...}''. This sign is only needed
in the on-line typing for avoiding the effect of the carriage return.
 The chain of characters which follow the {\tt //} is not interpreted 
(it is a comment line).

\noindent\dotfill

\input{diary/d1p2.dia}

We get the list of previously defined variables {\tt a b c A}  together
with the initial environment composed of the different libraries and
some specific ``permanent'' variables.

Below is an example of an expression which mixes constants with existing
variables.  The result is retained in the standard default variable 
{\tt ans}\index{ans@{\tt ans}}.

\noindent\dotfill

\input{diary/d1p2bis.dia}

Defining {\tt I}, a vector of indices, {\tt W} a random 2 x 4 matrix,
and extracting submatrices from {\tt W}. The \verb!$! symbol stands
for the last row or last column index of a matrix or vector. The colon
symbol stands for ``all rows'' or ``all columns''.

\noindent\dotfill

\input{diary/d1p3.dia}

Calling a function (or primitive) with a vector argument.  The response
is a complex vector.

\noindent\dotfill

\input{diary/d1p4.dia}

A  more complicated command which creates a polynomial. 

\noindent\dotfill

\input{diary/d1p41.dia}

Definition of a structure variable. 

\noindent\dotfill

\input{diary/d1p5.dia}

Definition of a polynomial matrix. The syntax for polynomial matrices
is the same as for numerical matrices. Calculation of the
determinant of the polynomial matrix by the {\tt det} function. 


\noindent\dotfill

\input{diary/d1p6.dia}

Definition of a matrix of rational polynomials. (The internal representation
of {\tt F} is a typed list of the form {\tt tlist('the type',num,den)}
where {\tt num} and {\tt den} are two matrix polynomials). Retrieving
the numerator and denominator matrices of {\tt F} by extraction operations in a
typed list. Last command is the direct extraction of entry {\tt 1,2} 
of the numerator matrix {\tt F.num}.

\noindent\dotfill

\input{diary/d1p7.dia}


Here we move into a new environment using the command 
{\tt pause}\index{pause@{\tt pause}}
and we obtain the new prompt {\tt -1->} which indicates the level
of the new environment (level 1).  All variables that are available
in the first environment are also available in the new environment.  Variables
created in the new environment can be returned to the original environment
by using {\tt return}\index{return@{\tt return}}.  
Use of {\tt return} without an argument 
destroys all the variables created in the new environment before returning
to the old environment. The {\tt pause} facility is very useful 
for debugging purposes.

\noindent\dotfill

\input{diary/d1p8.dia}

Definition of a rational polynomial by extraction of an entry
of the matrix {\tt F} defined above.  This is followed by the evaluation
of the rational polynomial at the vector of complex frequency values defined
by {\tt frequencies}.  The evaluation of the rational polynomial is done by
the primitive {\tt freq}. {\tt F12.num} is the numerator
polynomial and {\tt F12.den} is the denominator polynomial of the
rational polynomial {\tt F12}. Note that
the polynomial {\tt F12.num} can be also obtained by extraction
from the matrix {\tt F} using the syntax {\tt F.num(1,2)}.
The  visualization of the resulting evaluation
is made by using the basic plot command {\tt plot2d} (see Figure~\ref{f1.1}).

\noindent\dotfill

\input{diary/d1p9.dia}

The function {\tt horner} performs a (possibly symbolic) change of 
variables for a polynomial (for example, here, to
perform the bilinear transformation f(w(s))).

\noindent\dotfill

\input{diary/d1p10.dia}

Definition of a linear system in state-space representation.
The function {\tt syslin} defines here the continuous time ({\tt 'c'}) system
{\tt Sl} with state-space matrices ({\tt A,B,C}). The function
{\tt ss2tf} transforms {\tt Sl} into transfer matrix representation.

\noindent\dotfill

\input{diary/d1p11.dia}

Definition of the rational matrix {\tt R}. {\tt Sl} is the
continuous-time linear system with (improper) transfer matrix
{\tt R}. {\tt tf2ss} puts {\tt Sl} in state-space representation with a
polynomial {\tt D} matrix. Note that linear systems are represented
by specific typed lists (with 7 entries).

\noindent\dotfill

\input{diary/d1p12.dia}

{\tt sl1} is the linear system in transfer matrix representation
obtained by the parallel inter-connection of {\tt Sl} and {\tt 2*Sl +eye()}.
The same syntax is valid with {\tt Sl} in state-space representation.

\noindent\dotfill

\input{diary/d1p13.dia}

On-line definition of a function, called {\tt compen} which calculates the 
state space representation
({\tt Cl}) of a linear system ({\tt Sl}) controlled by an observer
with gain {\tt Ko}
and a controller with gain {\tt Kr}.  Note that matrices are constructed
in block form using other matrices.

\noindent\dotfill

\input{diary/d1p14.dia}

Call to the function {\tt compen} defined above where the gains were
calculated by a call to the primitive {\tt ppol} which performs pole
placement.
The resulting {\tt Aclosed} matrix is displayed and the placement
of its poles is checked using the primitive {\tt spec} which calculates
the eigenvalues of a matrix. (The function {\tt compen} is defined here
on-line by  as an example of function which receive a linear system 
({\tt Sl}) as input and returns a linear system ({\tt Cl}) as output.
In general Scilab functions are defined in files and loaded in Scilab
by {\tt exec} or by {\tt getf} ).

\noindent\dotfill

\input{diary/d1p15.dia}

Relation with the host environment.

\noindent\dotfill

\input{diary/d1p16.dia}

Definition of a column vector of character strings used for defining a C
function file. The routine is compiled (needs a compiler), 
and a shared library is done. The libary is dynamically 
linked to Scilab by the {\tt link} command, and interactively called
by the function {\tt myplus}. {\tt myplus} passes variables from Scilab
to C and conversely.

\noindent\dotfill

\input{diary/d1p17.dia}

Definition of a function which calculates a first order vector differential
{\tt f(t,y)}.  This is followed by the definition of the constant {\tt a}
used in the function.  The primitive {\tt ode}
then integrates the differential equation defined by the Scilab
function {\tt f(t,y)}
for {\tt y0=[1;0]} at {\tt t=0} and where the solution is given
at the time values $t=0,.02,.04,\ldots,20$. (Function {\tt f} can
be defined as a C or Fortran program). The result is plotted in
Figure~\ref{f1.2} where the first element of the integrated vector is 
plotted against the second element of this vector.

\noindent\dotfill

\input{diary/d1p18.dia}

Definition of a matrix containing character strings. By default, the 
operation of symbolic multiplication of two matrices of character
strings is not defined in Scilab.  However, the (on-line)
function definition for {\tt \%cmc} defines the multiplication of 
matrices of character strings.
The {\tt \%} which begins the function definition for {\tt \%cmc}
allows the definition of an operation which did not previously 
exist in Scilab, and the name {\tt cmc} means ``chain multiply chain''.
This example is not very useful: it is simply given to show
how {\em operations} such as \verb!*! can be defined on complex data 
structures by mean of scpecific Scilab functions.

\noindent\dotfill

\input{diary/d1p19.dia}

A simple example which illustrates the passing of a function as an argument
to another function. Scilab functions are objects which may be defined, loaded,
or manipulated as other objects such as matrices or lists.

\noindent\dotfill

\begin{verbatim}
-->quit
\end{verbatim}

Exit from Scilab.

\noindent\dotfill

\input{figures/d1-7.tex}
\caption{\label{f1.1}A Simple Response}
\end{figure}

\input{figures/d1-14.tex}
\caption{\label{f1.2}Phase Plot}
\end{figure}

