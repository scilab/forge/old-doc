\chapter{Basic Primitives}
\label{ch5}
This chapter briefly describes some basic primitives of Scilab.
More detailed information is given in the ``manual'' document.

\section{The Environment and Input/Output}
	In this chapter we describe the most important aspects
of the environment of Scilab: how to automatically
perform certain operations when entering Scilab,
and how to read and write data
from and to the Scilab environment.

\subsection{The Environment}
\label{s5.1}
\index{environment}

 Scilab is loaded with a number of variables and primitives.
The command {\tt who}\index{who@{\tt who}} lists the variables 
which are available. {\tt whos()}\index{whos@{\tt whos}} lists the variables 
which are available in a more detailled fashion.  

The {\tt who} command also indicates how many elements and variables
are available for use.  The user can obtain on-line help on any of 
the functions listed by typing {\tt help <function-name>}\index{help@{\tt help}}.

Variables can be saved in an external binary
file using {\tt save}\index{save@{\tt save}}.  
Similarly, variables previously saved can be
reloaded into Scilab using {\tt load}\index{load@{\tt load}}.

Note that after the command {\tt clear x y}\index{clear@{\tt clear}} 
 the variables {\tt x} and
{\tt y} no longer exist in the environment.  
The command {\tt save} without any variable arguments saves the entire 
Scilab environment.  Similarly,
the command {\tt clear} used
without any arguments clears all of the variables, functions, and libraries
in the environment.

Libraries of functions are loaded
using {\tt lib}\index{lib@{\tt lib}}\index{libraries}.  

The list of functions available in the library can be obtained by using {\tt disp}.

\subsection{Startup Commands by the User}
\label{s5.2}
\index{startup by user}
\index{startup@{\tt startup}}

	When Scilab is called the user can automatically load
into the environment functions, libraries, variables, and perform
commands using the the file {\tt .scilab} in his home directory.  
This is particularly useful when the user wants to run Scilab programs
in the background (such as in batch mode).  Another useful aspect
of the {\tt .scilab} file is when some functions or libraries
are often used.  In this case the commands {\tt getf} {\tt exec} or
{\tt load} can be used
in the {\tt .scilab} file to automatically load the desired 
functions and libraries whenever Scilab is invoked.

\subsection{Input and Output}
\label{s5.3}
\index{input}
\index{output}

	Although the commands {\tt save} and {\tt load} are
convenient, one has much more control over the transfer of
data between files and Scilab by using the Fortran like functions 
{\tt read}\index{read@{\tt read}}
and {\tt write}\index{write@{\tt write}}.  
These two functions work similarly to the
read and write commands found in Fortran.  The syntax of these
two commands is as follows.  
\begin{verbatim}
 
--> x=[1 2 %pi;%e 3 4]
 x         =
 
!   1.           2.    3.1415927 !
!   2.7182818    3.    4.        !
 
--> write('x.dat',x)
 
--> clear x
 
--> xnew=read('x.dat',2,3)
 xnew      =
 
!   1.           2.    3.1415927 !
!   2.7182818    3.    4.        !
\end{verbatim}
Notice that {\tt read} specifies the number of rows and columns
of the matrix {\tt x}. Complicated formats can be specified. 

The C like function {\tt mfscanf} \index{mfscanf@{\tt mfscanf}} and {\tt
mfprintf}\index{mfprintf@{\tt mfprintf}}  can be also used
\begin{verbatim}
 
--> x=[1 2 %pi;%e 3 4]
 x         =
 
!   1.           2.    3.1415927 !
!   2.7182818    3.    4.        !
 
--> fd=mopen('x_c.dat','w')

--> mfprintf(fd,'%f %f %f\n',x)

--> mclose(fd)
 
--> clear x
 
--> fd=mopen('x_c.dat','r')

--> xnew(1,1:3)=mfscanf(fd,'%f %f %f\n') ;

--> xnew(2,1:3)=mfscanf(fd,'%f %f %f\n')

 xnew  =
 
!   1.          2.    3.141593 !
!   2.718282    3.    4.       !
--> mclose(fd)
\end{verbatim}

Here is a table of useful input-output functions:

\begin{center}
\begin{tabular}{|c|c|}
\hline
\verb!mprintf! & print in standard output\\ \hline

\verb!mfprintf! & print in a file\\ \hline

\verb!msprintf! & print in a string matrix \\ \hline

\verb!mscanf! & read in standard input \\ \hline

\verb!mfscanf! & read in a file \\ \hline

\verb!msscanf! & read in a string matrix \\ \hline

\verb!fprintfMat! & formated write a matrix into a file  \\ \hline

\verb!fscanfMat! & formated read of a matrix in a file \\ \hline

\verb!mgetl! & read a file as a Scilab string matrix  \\ \hline

\verb!mputl! & write a string matrix \\ \hline

\verb!mopen! & open a file \\ \hline

\verb!mclose! & close a file\\ \hline
\end{tabular}
\end{center}

To manipulate binary files, the following functions are available:
\begin{center}
\begin{tabular}{|c|c|}
\hline
\verb!mget! & read binary data\\ \hline

\verb!mput! & write binary data\\ \hline

\verb!mgetstr! & print in a string matrix \\ \hline

\verb!mputstr! & write a string matrix \\ \hline

\verb!mgetstr! & read in a file \\ \hline

\verb!mtell! & current position in a file\\ \hline

\verb!mseek! & move current position  \\ \hline

\verb!meof! & end of file test \\ \hline
\end{tabular}
\end{center}

\section{Help}
On-line help is available either by clicking on the 
{\tt help}\index{help@{\tt help}}
button or by entering {\tt help item} (where {\tt item} is usually the 
name of a function or primitive). {\tt apropos
keyword}\index{apropos@{\tt apropos}} looks 
for {\tt keyword} in a {\tt whatis} file. 

 To add a new item or keyword is easy.
Just create a {\tt .cat} ASCII file describing the item and a 
{\tt whatis}\index{whatis@{\tt whatis}} file in your directory. 
Then add your directory path
(and a title) in the variable \verb!%helps! (see also the README file
there). You can use the standard format of the scilab manual\index{manual}
(see the {\tt SCIDIR/man/subdirectories} 
and {\tt SCIDIR/examples/man-examples}).
The Scilab \LaTeX\   manual is automatically obtained from the
manual items by a {\tt Makefile}. 
See the directory {\tt SCIDIR/man/Latex-doc}. 

\section{Useful functions}

We give here a short list of useful functions and keywords that can be used
as entry points in the Scilab manual. All the functions available 
can be obtained by entering {\tt help}. For each manual entry the
{\tt SEE ALSO} line refers to related functions.

\begin{itemize}
\item{Elementary functions: {\tt sum, prod, sqrt, diag, cos, max, round, sign, fft}}
\item{Sorting: {\tt sort, gsort, find}}
\item{Specific Matrices: {\tt zeros, eye, ones, matrix}}
\item{Linear Algebra: {\tt det, inv, qr, svd, bdiag, spec, schur}}
\item{Polynomials}: {\tt poly, roots, coeff, horner, clean, freq}
\item{Buttons, dialog: \verb!x_choose, x_dialog, x_mdialog, getvalue, addmenu!}
\item{GUI (Tcl-tk): \verb!TK_EvalStr, TK_GetVar, TK_SetVar, TK_EvalFile!}
\item{Linear systems: {\tt syslin}}
\item{Random numbers: {\tt rand}, {\tt grand}}
\item{Programming: {\tt function, deff, argn, for, if, end, while, 
select, warning, error, break, return}}
\item{Comparison symbols: {\tt ==}, {\tt >=}, {\tt >}, {\tt ~=}, {\verb!&!}
({\tt and}),{\tt |} ({\tt or})}
\item{Execution of a file: {\tt exec}}
\item{Debugging: {\tt pause, return, abort}}
\item{Spline functions, interpolation: {\tt splin2d, interp2d, smooth, splin3d}}
\item{Character strings: {\tt string, part, evstr, execstr, grep}}
\item{Graphics: {\tt plot2d, set, get, xgrid, locate, plot3d, Graphics}}
\item{Ode solvers: {\tt ode, dassl, dassrt, odedc}}
\item{Optimization: {\tt optim, quapro, linpro, lmitool, lsqrsolve}}
\item{Interconnected dynamic systems: {\tt scicos}}
\item{Adding a C or Fortran routine: {\verb!link, call, addinter, ilib_for_link, ilib_build!}}
\item{Graphs, networks: {\verb! edit_graph, metanet!}}
\end{itemize}


\section{Nonlinear Calculation}
\label{ch6}
\index{non-linear calculation}

	Scilab has several powerful non-linear primitives for simulation
or optimization.
\index{simulation}
\index{optimization}
\subsection{Nonlinear Primitives}
Scilab provides several facilities for nonlinear calculations.

Numerical simulation of systems of differential equations is
made by the {\tt ode} primitive. Many solvers are available,
mostly from {\tt odepack}, for solving stiff or non-stiff systems.
Implicit systems can be solved by {\tt dassl}. It is also
possible to solve systems with stopping time: integration
is performed until the state is crossing a given surface.
See {\tt ode} and {\tt dassrt} commands. There is a number
of optional arguments available for solving ode's (tolerance
parameters, jacobian, order of approximation, time steps etc).
For {\t ode} solvers, these parameters are set by the global 
variable {\tt \%ODEOPTIONS}.

Minimizing non linear functions is done the {\tt optim} function. 
Several algorithms (including non differentiable optimization)
are available. Codes are from INRIA's {\tt modulopt} library.
Enter {\tt help optim} for more a more detailed description.

\subsection{Argument functions}
\label{refexternals}
\index{external}
\index{argument function}
Specific Scilab functions or C or Fortran routines can be used as an argument
of some high-level primitives (such as {\tt ode}, {\tt optim}, {\tt dassl}...).
These fonctions are called argument functions or externals.
The calling sequence of this function or routine is imposed by
the high-level primitive which sets the argument of this function or
routine.

For example the function {\tt costfunc} is an argument of 
the {\tt optim} primitive. Its calling sequence must be: 
{\tt [f,g,ind]=costfunc(x,ind)} as imposed by the {\tt optim} primitive.
The following non-linear primitives in Scilab need argument functions
or subroutines: 
{\tt ode}, {\tt optim}, {\tt impl}, {\tt dassl}, {\tt intg}, 
{\tt odedc}, {\tt fsolve}.
For problems where computation time is important, it is recommended
to use C or Fortran subroutines. Examples of such
subroutines are given in the directory {\tt SCIDIR/routines/default}.
See the README file there for more details.

When such a subroutine is written it must be linked to Scilab.
This link operation can be done dynamically by the {\tt link}
command. It is also possible to introduce the code in a more 
permanent manner by inserting it in a specific interface in 
{\tt SCIDIR/routines/default} and rebuild a new Scilab by a 
{\tt make all} command in the Scilab directory.

\section{XWindow Dialog}
It may be convenient to open a specific XWindow window for entering
interactively parameters inside a function or for a demo.
This facility is possible thanks to e.g. the functions \verb!x_dialog!,
\verb!x_choose!, \verb!x_mdialog!, \verb!x_matrix! and \verb!x_message!.
The demos which can be executed by clicking on the {\tt demo} button
provide simple examples of the use of these functions.

\section{Tk-Tcl Dialog}
An interface between Scilab and Tk-Tcl exists. 
A Graphic User Interface object can be created by the function
\verb!uicontrol!. Basic primitives are \verb!TK_EvalFile!,
\verb!TK_EvalStr! and \verb! TK_GetVar, TK_Setvar!. Examples
are given by invoking the help of these functions.

Let us give a simple dialog. We pass a script to TK as a Scilab
string matrix, TK opens a dialog box, and the result is returned
to Scilab as a string, using \verb!TK_GetVar!.
\begin{verbatim}
-->TK_script=["toplevel .dialog";
              "label .dialog.1 -text ""Enter your input\n here""";
              "pack .dialog.1";
              "entry .dialog.e -textvariable scivar";
              "set scivar """"";
              "pack .dialog.e"];
-->TK_EvalStr(TK_script);
-->text=TK_GetVar(scivar);
\end{verbatim}

